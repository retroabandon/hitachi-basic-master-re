Hitachi Basic Master Reverse Engineering
========================================

This repo contains design documents and code from the Hitachi BASIC Master
([日立　ベーシックマスター][wp-ja]) series of computers.

The models using a Motorola 6800 CPU were:
- MB-6880 Basic Master (1978-9): 8K RAM, 8K ROM
- MB-6880L2  Basic Master Level 2 (1979-2) 8K RAM, 16K ROM
- MB-6881 Basic Master Level 2 II (1980): 16K RAM, 16K ROM
- MB-6885 Basic Master Jr. (1981): 16K RAM (max 63.5K), 16K ROM

The models using a Motorola 6809 CPU were:
- MB-6890 Basic Master Level 3 (1980-5)
- MB-6890 Basic Master Level 3 MarkII (1982-4)
- MB-6890 Basic Master Level 3 Mark5 (1983-5)

The `bm2/` subdirectory contains a copy of the bm2 emulator for the MB-6885
(Basic Master Jr.). See [bm2/README](./bm2/README.md) for details.


BASIC Master Jr (MB-6885) ROMs
------------------------------

Initial versions of ROMs saved from MB-6885 and decoded by WIP CMT tools.
- `MON.bin`: save of 4K of MONITOR ROM
- `PRINTER.bin`: save of 4K of PRINTER ROM
- `BASIC.bin`: save of single 12K of entire BASIC ROM area


Disassembly System
------------------

This project uses [f9dasm] for disassembly, but our default submodule
configuration uses [cjs's fork][f9dasm-cjs] of it to include useful patches
not in the upstream disassembler. The build system is substantially similar
to the [retroabandon/fm7re] and [retroabandon/panasonic-jr] projects. Run
`make` to run the disassembly.

The disassemblies, `*.dis` files, are committed to the repo for easy
viewing through web interfaces to the repo. These are generated from
`*.bin` (binary code) and `*.info` (annotation) files, with
`common.info` holding common information used in multiple
disassemblies.

The documentation for the `.info` file syntax can be viewed on-line
[here][f9dasm-doc].

Please ensure that if you set options for more debugging information in the
disassembly output (usually the ones in the top-level `common.info`) you
disable them again and re-run the disassembly before commiting the new
disassembly output.


Hardware Notes
--------------

There are a few notes in [`sedoc:8bit/hitachi/mb-6885`][sedoc] as well.

`doc/IO_198203_BasicMasterJr.pdf` contains the Basic Master Jr. schematic
and a few other relevant pages from the 1982-03 issue of _I/O_ magazine.

#### Keyboard

The Alps keyboard PCB supplies the matrix rows/columns on two connectors,
`E` (13 pins) and `F` (12 pins). (The adjacent `C` connector goes to the
speaker.)

    F1  row 3 (1234...)     RD $EEC0 b3 low-active
    F2  row 2 (QWER...)     "    "   b2    "
    F3  row 1 (ASDF...)     "    "   b1    "
    F4  row 0 (ZXCV...)     "    "   b0    "
    F5  英数 key            "    "   b4    "
    F6  英数号 key          "    "   b5    "
    F7  カナ記号 key        "    "   b6    "
    F8  カナ key            "    "   b7    "
    F9  BREAK/RESET key
    F11 GND for other side of F5-F9 switches

    E1  col  0 (ZAQ1)       WR $EEC0 b3-0: $0
    E2  col  1 (XSW2)       "    "     "   $1
    E3  col  2 (CDE3)       "    "     "   $2
    E4  col  3 (VFR4)       "    "     "   $3
    E5  col  4 (BGT5)       "    "     "   $4
    E6  col  5 (NHY6)       "    "     "   $5
    E7  col  6 (MJU7)       "    "     "   $6
    E8  col  7 (,KI8)       "    "     "   $7
    E9  col  8 (.LO9)       "    "     "   $8
    E10 col  9 (/;P0)       "    "     "   $9
    E11 col 10 (_:@-)       "    "     "   $A
    E12 col 11 (    )       "    "     "   $B
    E13 col 12 (    )       "    "     "   $C

Writes to address $EEC0 (and other addrs?) go to a '174 hex D flip-flop
IC39. Data bus `D0-D3` are latched by flip-flops 1-4 for a pair of '138
decoders IC37, IC38 which select low one of `E1-E13`, the rest being held
high. Flip-flop 5 is unused.

Data bus `D7` is latched through the 6th flip-flop. This is inverted again
via IC19 before reaching one of the inputs of a 2-input NOR ('02, IC24).
The other input is from `F9` (BREAK/RESET key) via a Schmitt-trigger and
regular inverter. Thus only when the breapk key is pressed _and_ $EEC0
bit 7 has been written high (taking both inputs low) will the NOR output
(written here as the equivalent AND with inverted inputs) go high, causing
a low edge (via an inverter on '05 IC14) on the NMI line. This signal also
goes via a gate used as an inverter on IC24 to another flip-flop ('74 IC21)
which is presumably used for detecting the NMI source.

A separate circuit similar to the above, but combining `F9` BREAK and `F7`
カナ記号 brings RESET low. (There is no flip-flop to disable this.)

Previous notes just from looking at traces:
- It appears that various pins on `E` are connected to the `Y0…Y7` pins of
  a pair of 74LS138 demultiplexers (`IC37` @H15, `IC38` @H16) that might
  set rows (columns?) high or low. A few traces lead from `IC38` to a
  74LS174 hex D flip-flop (`IC39` @G16), which may hold the values for for
  the `A,B,C` inputs on the demuxes.
- `F` has no obvious traces on the top side of the PCB except for what
  might be power and ground.
- There are 26 resistors (`CP2`? and `R99`-`R106` @K15) that may be
  pull-ups or pull-downs for keyboard lines.
- Not sure what the 74LS367A hex bus drivers (`IC36` @K16, `IC44` @J15,
  `IC45` @J16) in that area are for.



<!-------------------------------------------------------------------->
[wp-ja]: https://ja.wikipedia.org/wiki/ベーシックマスター

[f9dasm-cjs]: https://github.com/0cjs/f9dasm
[f9dasm-doc]: https://htmlpreview.github.io/?https://github.com/Arakula/f9dasm/blob/master/f9dasm.htm
[f9dasm]: https://github.com/Arakula/f9dasm
[radare2]: https://github.com/0cjs/sedoc/tree/master/app/radare2.md
[retroabandon/fm7re]: https://gitlab.com/retroabandon/fm7re
[retroabandon/hitachi-basic-master-re]: https://gitlab.com/retroabandon/hitachi-basic-master-re
[retroabandon/panasonic-jr]: https://gitlab.com/retroabandon/panasonic-jr

[sedoc]: https://github.com/0cjs/sedoc/blob/master/8bit/hitachi/6885.md
