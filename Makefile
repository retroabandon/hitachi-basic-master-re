DISASSEMBLIES := $(wildcard */*.dis)

all: $(DISASSEMBLIES) bm2/bm2/readme.txt
.PHONY: all

MAKEFLAGS += --no-builtin-rules		# Makes -d output _much_ shorter.

%.dis: %.bin %.info common.info f9dasm/f9dasm
	f9dasm/f9dasm -cchar ';' -out $@ -info $*.info $<
	bin/f9post $@

bm2/bm2/readme.txt: bm2/Archive/bm2src_20161113.tgz
	tar -x -C bm2/ -f $<
	@#   Make target newer so we need not extract again.
	touch $@

#   f9dasm currently has no build system, but building it ourselves here
#   might be easier than using one provided by that repo anyway.
#   `ignore = untracked` in ../.gitmodules avoids Git seeing the submodule
#   as modified because we build `f9dasm` in the source directory.
f9dasm/f9dasm: f9dasm/f9dasm.c
	cc -o f9dasm/f9dasm f9dasm/f9dasm.c

#   Check out the submodule if that's not already been done.
f9dasm/f9dasm.c:
	git submodule update --init f9dasm

